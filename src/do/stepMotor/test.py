#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Created on Dec 26, 2015

@author: Denis
'''

import time
from src.libs.device.stepMotor.SM_28BYJ_48 import SM_28BYJ_28
from src.libs.port.port import PortID

if __name__ == '__main__':
    sm = SM_28BYJ_28([PortID.PA20, PortID.PA10, PortID.PA9, PortID.PA8])
    time.sleep(1)
    sm.rotate_degree(360, 45)
    sm.rotate_degree(360, 30)
    sm.rotate_degree(360, 10)
    sm.rotate_degree(360, 30)
    sm.rotate_degree(360, 58)
    sm.rotate_degree(360)
    sm.rotate_degree(360, 15)
    sm.stop(True)
