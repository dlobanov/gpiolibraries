'''
Created on Dec 4, 2015

@author: Denis
'''

import os
from enum.enum import Enum

BASE_PORTS_PATH = "/sys/class/gpio_sw/"
DATA_FILE = "data"
CONFIG_FILE = "cfg"


class PortException(Exception):
    pass


class PortStatus(Enum):
    UP = "1"
    DOWN = "0"


class PortMode(Enum):
    IN = "0"
    OUT = "1"


class PortID(Enum):
    PA20 = "PA20"
    PA10 = "PA10"
    PA9 = "PA9"
    PA8 = "PA8"

    def __init__(self, port):
        if not os.path.exists(os.path.sep.join([BASE_PORTS_PATH, port])):
            raise PortException("Port {0} does not exists".format(port))
        self.cfg_file = os.path.sep.join([BASE_PORTS_PATH,
                                          port,
                                          CONFIG_FILE])
        self.data_file = os.path.sep.join([BASE_PORTS_PATH,
                                           port,
                                           DATA_FILE])

    def get_cfg_file(self):
        return self.cfg_file

    def get_data_file(self):
        return self.data_file


class Port(object):

    def __init__(self, port, mode=PortMode.OUT):
        if not isinstance(port, PortID):
            raise PortException("First parameter has to be instance of the "
                                "PortID")
        self.port = port
        self.status = None

        # open port data file
        self.data_file = open(self.port.get_data_file(), "w+", 0)

        # open port configuration file
        self.cfg_file = open(self.port.get_cfg_file(), "w", 0)
        self.set_port_mode(mode)

    def __del__(self):
        if hasattr(self, "cfg_file"):
            self.set_port_mode(PortMode.OUT)
            self.low()
            self.cfg_file.close()
        if hasattr(self, "data_file"):
            self.data_file.close()

    def set_port_mode(self, mode):
        if not isinstance(mode, PortMode):
            raise PortException("mode should be instance of PortMode")
        self.cfg_file.write(str(mode.value))
        self.mode = mode
        if mode == PortMode.OUT:
            self.low()

    def write(self, data):
        if self.mode != PortMode.OUT:
            raise PortException(
                "Unable to write to the data_file, unacceptable mode")
        self.data_file.write(data)

    def read_state(self):
        if self.mode != PortMode.IN:
            raise PortException(
                "Unable to read from the data_file, unacceptable mode")
        self.data_file.seek(0)
        return PortStatus(self.data_file.read().strip())

    def high(self):
        self.write(PortStatus.UP.value)
        self.status = True

    def low(self):
        self.write(PortStatus.DOWN.value)
        self.status = False

    def get_out_status(self):
        if self.mode != PortMode.OUT:
            raise PortException("Port is not in out mode")
        return self.status
