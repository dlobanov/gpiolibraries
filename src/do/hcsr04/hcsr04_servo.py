'''
Created on Dec 5, 2015

@author: Denis
'''
import time
from src.libs.device.echoDistanceMeter.hardware.hcsr04 import HC_SR04
from src.libs.device.servo.servo import Servo
from src.libs.device.servo.configs.sc_microservo_9g import MicroServo9G
from src.libs.port.port import PortID


def typeDistance(distance):
    print "Current distance is ", distance
    angle = distance * 100
    if angle > 180:
        angle = 180
    servo.set_angle(-90 + angle)

if __name__ == '__main__':
    servo = Servo(PortID.PA20, MicroServo9G(), -90, directDependency=False)
    dm = HC_SR04(PortID.PA9, PortID.PA10, 0.1)
    dm.connect("distance", typeDistance)
    dm.start_measuring()
    time.sleep(60)
    dm.stop_measuring()
    servo.stop(True)
