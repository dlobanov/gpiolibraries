#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Created on Dec 26, 2015

@author: Denis
'''

import time
from multiprocessing import Process, Value, Queue
from enum.enum import Enum
from src.libs.port.port import Port
from Queue import Empty


class StepMotorException(Exception):
    pass


class StepMotorDirection(Enum):
    RIGHT = 1
    LEFT = 0


class BaseStepMotor(Process):

    def __init__(self, ports, current):
        super(BaseStepMotor, self).__init__(target=self)
        if not isinstance(ports, list):
            raise StepMotorException("Ports should be a list")
        self.ports = []
        for p in ports:
            self.ports.append(Port(p))
        self.ports_num = len(self.ports)
        self.active = Value("b", True)
        self.rotate = Value("b", True)
        self.add_tasks = Value("b", True)
        self.queue = Queue()
        self.start()

    def rotate_steps(self, steps, timeout=0):
        """ Does several half-steps with timeout between them
        steps - number of half - steps to do
        timeout - timeout between half-steps, in seconds
        """
        if self.add_tasks.value:
            self.queue.put((steps, timeout))

    def stop(self, join=True, finish_taks=True, timeout=None):
        self.add_tasks.value = False
        if finish_taks:
            while self.queue.qsize() > 0:
                time.sleep(0.01)
        self.rotate.value = False
        self.active.value = False
        self.join(timeout=timeout)
        for port in self.ports:
            port.low()

    def _rotate_impl(self, steps, step_timeout=0):
        up = True if steps > 0 else False
        timeout = step_timeout
        if timeout < self.MIN_TIME:
            timeout = self.MIN_TIME
        for _ in xrange(0, abs(int(steps))):
            if self.rotate.value is False:
                break
            end_time = time.time() + timeout
            self._next_step(up)
            sleep_time = end_time - time.time()
            if sleep_time > 0:
                time.sleep(sleep_time)

    def _next_step(self, up):
        active_ports_ids = []
        for port_id in xrange(0, self.ports_num):
            if self.ports[port_id].get_out_status():
                active_ports_ids.append(port_id)
        active_ports_num = len(active_ports_ids)
        if active_ports_num == 0:
            self.ports[0].high()
        elif active_ports_num == 1:
            if up:
                if active_ports_ids[0] + 1 >= self.ports_num:
                    self.ports[0].high()
                else:
                    self.ports[active_ports_ids[0] + 1].high()
            else:
                if active_ports_ids[0] >= 0:
                    self.ports[active_ports_ids[0] - 1].high()
                else:
                    self.ports[self.ports_num - 1].high()
        elif active_ports_num == 2:
            if up:
                if active_ports_ids[1] == self.ports_num - 1:
                    # 0 and last-1 are possible
                    if active_ports_ids[0] == 0:
                        self.ports[self.ports_num - 1].low()
                    else:
                        self.ports[active_ports_ids[0]].low()
                else:
                    self.ports[active_ports_ids[0]].low()
            else:
                if active_ports_ids[1] == self.ports_num - 1:
                    # 0 and last-1 are possible
                    if active_ports_ids[0] == 0:
                        self.ports[0].low()
                    else:
                        self.ports[self.ports_num - 1].low()
                else:
                    self.ports[active_ports_ids[1]].low()

    def run(self):
        while self.active.value:
            try:
                (steps, timeout) = self.queue.get(block=False)
                self._rotate_impl(steps, timeout)
            except Empty:
                pass
