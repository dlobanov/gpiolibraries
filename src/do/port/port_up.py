'''
Created on Dec 4, 2015

@author: Denis
'''

import time
from src.libs.port.port import Port, PortID

if __name__ == '__main__':
    port = Port(PortID.PA10)
    port.high()
    time.sleep(2)
