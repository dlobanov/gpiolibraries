'''
Created on Dec 4, 2015

@author: Denis
'''

import time
from src.libs.port.port import Port, PortID

BLINK_TIME = 0.2
BLINKS = 10

if __name__ == '__main__':
    port = Port(PortID.PA10)
    for i in xrange(1, BLINKS):
        port.high()
        time.sleep(BLINK_TIME)
        port.low()
        time.sleep(BLINK_TIME)
