#!/usr/bin/env python
'''
Created on Dec 4, 2015

@author: Denis
'''

import time
from src.libs.device.servo.servo import Servo
from src.libs.device.servo.configs.sc_microservo_9g import MicroServo9G
from src.libs.port.port import PortID

if __name__ == '__main__':
    print "Angle 0"
    servo = Servo(PortID.PA20, MicroServo9G(), directDependency=False)
    time.sleep(3)
    print "Angle -90"
    servo.set_angle(-90)
    time.sleep(3)
    print "Angle 90"
    servo.set_angle(90)
    time.sleep(3)
    print "Angle 0"
    servo.set_angle(0)
    time.sleep(3)
    servo.stop(True)
