'''
Created on Dec 5, 2015

@author: Denis
'''

from multiprocessing import Process, Value
from src.libs.core.signal_slot import SignalSlot


class DistanceMetterException(Exception):
    pass


class DistanceMetter(Process, SignalSlot):

    def __init__(self, updateInterval=1):
        self._init_signal()
        super(DistanceMetter, self).__init__(target=self)

        self.updateInterval = Value("d")
        self.setUpdateInterval(updateInterval)

        self.distance = Value("d", -1)

    def getDistance(self):
        return self.distance.value

    def _setDistance(self, distance):
        self.distance.value = distance
        self.emit("distance", distance)

    def setUpdateInterval(self, updateInterval):
        if hasattr(self, ""):
            if updateInterval < self.UPDATE_INTERVAL_MIN:
                raise DistanceMetterException("Unable to set update interval "
                                              "to {0}, minimal update "
                                              "interval for this device "
                                              "is {1}".format(
                                                  updateInterval,
                                                  self.UPDATE_INTERVAL_MIN
                                              ))
        self.updateInterval.value = updateInterval

    def getUpdateInterval(self):
        return self.updateInterval.value

    def run(self):
        raise DistanceMetterException("Run method is not "
                                      "overridden")
