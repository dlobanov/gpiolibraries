'''
Created on Dec 5, 2015

@author: Denis
'''
import time
from src.libs.device.echoDistanceMeter.hardware.hcsr04 import HC_SR04
from src.libs.port.port import PortID


def typeDistance(distance):
    print "Current distance is ", distance

if __name__ == '__main__':
    dm = HC_SR04(PortID.PA9, PortID.PA10, 0.1)
    dm.connect("distance", typeDistance)
    dm.start_measuring()
    time.sleep(60)
    dm.stop_measuring()
