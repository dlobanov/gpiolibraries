'''
Created on Dec 4, 2015

@author: Denis
'''

import time
from multiprocessing import Process, Value
from src.libs.device.servo.configs.base_servo_config import BaseServoConfig
from src.libs.port.port import Port


class ServoException(Exception):
    pass


class Servo(Process):

    def __init__(self, port, servoConfig, angle=0, start=True,
                 directDependency=True):
        super(Servo, self).__init__(target=self)
        if not isinstance(servoConfig, BaseServoConfig):
            raise ServoException("Please provide correct servo config as "
                                 "second argument")

        self.port = Port(port)
        self.servoConfig = servoConfig

        self.time_per_degree = \
            (servoConfig.CONTROLL_MAX_TIME - servoConfig.CONTROLL_MIN_TIME) / \
            (servoConfig.CONTROLL_MAX_ANGLE - servoConfig.CONTROLL_MIN_ANGLE)

        self.directDependency = directDependency

        self.up_time = Value('d', 0)
        self.active = Value('b', start)
        self.set_angle(angle)
        if self.active.value:
            self.start()

    def __del__(self, timeout=None):
        self.stop(True)

    def set_angle(self, angle):
        if angle < self.servoConfig.CONTROLL_MIN_ANGLE or \
                angle > self.servoConfig.CONTROLL_MAX_ANGLE:
            raise ServoException("Angle should be more than {0} and "
                                 "less than {1} degree".format(
                                     self.servoConfig.CONTROLL_MIN_ANGLE,
                                     self.servoConfig.CONTROLL_MAX_ANGLE))
        time_delta = self.time_per_degree * \
            (angle - self.servoConfig.CONTROLL_MIN_ANGLE)
        if self.directDependency:
            self.up_time.value = self.servoConfig.CONTROLL_MIN_TIME + \
                time_delta
        else:
            self.up_time.value = self.servoConfig.CONTROLL_MAX_TIME - \
                time_delta

    def stop(self, join=False, timeout=None):
        self.active.value = False
        if join:
            self.join(timeout if timeout else
                      self.servoConfig.CONTROLL_SIGNAL_TIME * 2)

    def run(self):
        self.active.value = True
        while self.active.value:
            up = self.up_time.value
            start_cycle_time = time.time()
            self.port.high()
            time.sleep(up)
            self.port.low()
            sleep_time = start_cycle_time + \
                self.servoConfig.CONTROLL_SIGNAL_TIME - time.time() - up
            if sleep_time > 0:
                time.sleep(sleep_time)
