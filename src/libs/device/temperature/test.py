#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Created on Dec 31, 2015

@author: Den
'''

import time
from src.libs.port.port import Port, PortID, PortMode

if __name__ == '__main__':
    p = Port(PortID.PA20, PortMode.OUT)
    p.high()
    time.sleep(1)
    p.low()
    time.sleep(0.5)
    p.set_port_mode(PortMode.IN)
    for i in xrange(0, 100):
        print p.read_state()
        time.sleep(0.01)
