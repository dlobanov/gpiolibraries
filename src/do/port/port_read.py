'''
Created on Dec 4, 2015

@author: Denis
'''

import time
from src.libs.port.port import Port, PortMode


if __name__ == '__main__':
    port = Port("PA10", PortMode.IN)
    for i in xrange(0, 5):
        print "Read ", i
        print port.read_state()

        time.sleep(3)
