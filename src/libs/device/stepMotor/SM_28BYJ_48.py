#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Created on Dec 26, 2015

@author: Denis
'''

from src.libs.device.stepMotor.baseStepMotor import BaseStepMotor,\
    StepMotorException


class SM_28BYJ_28(BaseStepMotor):
    DEGREE_PER_STEP = 5.625 / 64
    MIN_TIME = 0.0015

    def __init__(self, ports, current=0):
        super(SM_28BYJ_28, self).__init__(ports, current)
        if self.ports_num != 4:
            raise StepMotorException("4 ports are required")

    def rotate_degree(self, angle, speed=0):
        print "RD in ", angle, speed
        timeout = 0
        if speed > 0:
            timeout = self.DEGREE_PER_STEP / speed
        steps = angle / self.DEGREE_PER_STEP
        print "RD ", steps, timeout
        self.rotate_steps(steps, timeout)
