#!/usr/bin/env python

from enum.enum import Enum


class TestPortMode(Enum):
    IN = 0
    OUT = 1


if __name__ == '__main__':
    print TestPortMode(0)
