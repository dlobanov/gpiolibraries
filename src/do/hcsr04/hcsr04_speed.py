'''
Created on Dec 5, 2015

@author: Denis
'''
import time
from src.libs.device.echoDistanceMeter.hardware.hcsr04 import HC_SR04
from src.libs.port.port import PortID

MEASURING_INTERVAL = 0.1


class Speed(object):

    DISTANCE = None

    def typeDistance(self, distance):
        if self.DISTANCE is not None:
            print "Current speed is ", abs((self.DISTANCE - distance) / MEASURING_INTERVAL)
        self.DISTANCE = distance

if __name__ == '__main__':
    speed = Speed()
    dm = HC_SR04(PortID.PA9, PortID.PA10, MEASURING_INTERVAL)
    dm.connect("distance", speed.typeDistance)
    dm.start_measuring()
    time.sleep(60)
    dm.stop_measuring()
