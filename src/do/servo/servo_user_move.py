#!/usr/bin/env python
'''
Created on Dec 4, 2015

@author: Denis
'''

from src.libs.device.servo.servo import Servo
from src.libs.device.servo.configs.sc_microservo_9g import MicroServo9G
from src.libs.port.port import PortID

if __name__ == '__main__':
    print "Angle 0"
    servo = Servo(PortID.PA20, MicroServo9G(), directDependency=False)
    while True:
        try:
            angle = int(raw_input("Input the angle "))
        except Exception:
            break
        if angle > 90 or angle < -90:
            break
        print "Setting angle {0}".format(angle)
        servo.set_angle(angle)
    servo.stop(True)
