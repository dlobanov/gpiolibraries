'''
Created on Dec 5, 2015

@author: Denis
'''

import time
from multiprocessing import Value
from src.libs.port.port import Port, PortStatus, PortMode
from src.libs.device.echoDistanceMeter.distanceMetter import DistanceMetter


class HC_SR04(DistanceMetter):
    UPDATE_INTERVAL_MIN = 0.06
    TRIGGER_TIME = 0.000001
    MAX_DISTANCE = 4

    def __init__(self, triggerPort, echoPort, updateInterval=1):
        super(HC_SR04, self).__init__()

        if updateInterval:
            self.setUpdateInterval(updateInterval)

        self.triggerPort = Port(triggerPort, PortMode.OUT)
        self.echoPort = Port(echoPort, PortMode.IN)

        self.measure = Value("b", False)

    def start_measuring(self):
        self.measure.value = True
        self.start()

    def stop_measuring(self):
        self.measure.value = False
        self.join(self.getUpdateInterval() * 2)

    def run(self):
        while self.measure.value:
            end_time = time.time() + self.getUpdateInterval()
            self.triggerPort.high()
            time.sleep(self.TRIGGER_TIME)
            self.triggerPort.low()
            echo_start = 0
            echo_down = 0
            while not echo_down:
                if not echo_start:
                    if self.echoPort.read_state() == PortStatus.UP:
                        echo_start = time.time()
                else:
                    if self.echoPort.read_state() == PortStatus.DOWN:
                        echo_down = time.time()
                if time.time() > end_time:
                    break
            if echo_start != 0 and echo_down != 0:
                dist = (echo_down - echo_start) * 340 / 2
                if dist <= self.MAX_DISTANCE:
                    self._setDistance(dist)
            sleep_time = end_time - time.time()
            if sleep_time > 0:
                time.sleep(sleep_time)
