#!/usr/bin/env python


class A(object):

    def test(self):
        print self.TEST


class B(A):
    TEST = "DATA"

if __name__ == '__main__':
    b = B()
    b.test()
