#!/usr/bin/env python
'''
Created on Dec 4, 2015

@author: Denis
'''

import time
from src.libs.device.servo.servo import Servo
from src.libs.device.servo.configs.sc_microservo_9g import MicroServo9G
from src.libs.port.port import PortID

if __name__ == '__main__':
    servo = Servo(PortID.PA20, MicroServo9G(), 90, directDependency=False)
    time.sleep(3)
    servo.stop(True)
