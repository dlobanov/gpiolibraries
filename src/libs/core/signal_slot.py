'''
Created on Dec 5, 2015

@author: Denis
'''


class SignalSlot(object):

    def _init_signal(self):
        if not hasattr(self, "_signals"):
            self._signals = {}

    def connect(self, signal, target):
        if signal not in self._signals:
            self._signals[signal] = []

        self._signals.get(signal).append(target)

    def emit(self, signal, *args, **kwargs):
        if signal in self._signals:
            for target in self._signals.get(signal):
                target(*args, **kwargs)
