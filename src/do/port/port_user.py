'''
Created on Dec 4, 2015

@author: Denis
'''

import time
from src.libs.port.port import Port, PortID, PortMode

if __name__ == '__main__':
    port = Port(PortID.PA20)
    while True:
        user_input = raw_input("Type action [r/w]")
        if user_input == "r":
            port.set_port_mode(PortMode.IN)
            print "Status is {0}".format(port.read_state())
        elif user_input == "w":
            port.set_port_mode(PortMode.OUT)
            port.high()
            time.sleep(1)
            port.low()
        else:
            print "Unknown action"
