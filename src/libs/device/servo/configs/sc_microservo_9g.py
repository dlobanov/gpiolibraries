'''
Created on Dec 4, 2015

@author: Denis
'''
from src.libs.device.servo.configs.base_servo_config import BaseServoConfig


class MicroServo9G(BaseServoConfig):
    CONTROLL_SIGNAL_TIME = 0.02
    CONTROLL_MIN_TIME = 0.00058
    CONTROLL_MAX_TIME = 0.0024
    CONTROLL_MIN_ANGLE = -90
    CONTROLL_MAX_ANGLE = 90
