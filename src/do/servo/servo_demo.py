#!/usr/bin/env python
'''
Created on Dec 4, 2015

@author: Denis
'''

import time
from src.libs.device.servo.servo import Servo
from src.libs.device.servo.configs.sc_microservo_9g import MicroServo9G
from src.libs.port.port import PortID

if __name__ == '__main__':
    servo = Servo(PortID.PA20, MicroServo9G(), -90, directDependency=False)
    angle = -90
    up = True
    min_angle = -90
    max_angle = 90
    angle_step = 1
    step_time = 0.02

    try:
        while True:
            if up:
                angle += angle_step
                if angle > max_angle:
                    up = False
                    angle = max_angle
            else:
                angle -= angle_step
                if angle < min_angle:
                    up = True
                    angle = min_angle
            servo.set_angle(angle)
            time.sleep(step_time)
    except KeyboardInterrupt:
        servo.stop(True)
